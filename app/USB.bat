:: Run in silence mode
ECHO OFF
cls
:: In windows 10 devcon needs admin to restart USB
ECHO Waiting for start
TIMEOUT 10
ECHO Trying to restart all USB Devices
"%~dp0devcon.exe" restart *ROOT_HUB*
TIMEOUT 20