//@author: ethermammoth
//@help: template for standard shaders
//@tags: template
//@credits: 

Texture2D texture2d <string uiname="Texture";>;
Texture2D textureblend <string uiname="Texture Blend";>;

SamplerState linearSampler : IMMUTABLE
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};
 
cbuffer cbPerDraw : register( b0 )
{
	float4x4 tVP : VIEWPROJECTION;	
};

cbuffer cbPerObj : register( b1 )
{
	float4x4 tW : WORLD;
	float4 cAmb <bool color=true;String uiname="Color";> = { 1.0f,1.0f,1.0f,1.0f };
	float4 gColor1 <bool color=true;> =float4(0,0,0,1);
	float4 gColor2 <bool color=true;> =1;
	float Gamma=1;
	float Fader <float uimin=0.0;float uimax=1.0;> =1.0;
};

cbuffer cbTextureData : register(b2)
{
	float4x4 tTex <string uiname="Texture Transform"; bool uvspace=true; >;
	float4x4 gTexTransform <string uiname="Gradient Transform"; bool uvspace=true; >;
};

struct VS_IN
{
	float4 PosO : POSITION;
	float4 TexCd : TEXCOORD0;
};

struct vs2ps
{
    float4 PosWVP: SV_POSITION;
    float4 TexCd: TEXCOORD0;
	float4 TexCd2 : TEXCOORD1;
};

vs2ps VS(VS_IN input)
{
    vs2ps output;
    output.PosWVP  = mul(input.PosO,mul(tW,tVP));
    output.TexCd = input.TexCd;
	output.TexCd2 = mul(input.TexCd, tTex);
    return output;
}

//PS Gradient
float4 pGRAD(float4 PosWVP,float2 x){
	float4 c=0;
	float2 x0=mul(float4((x.xy*2-1)*float2(1,-1),0,1), gTexTransform).xy*0.5*float2(1,-1)+0.5;
	float fade=x0.x;
	fade=sign(fade)*pow(abs(fade),Gamma);
	c=lerp(gColor1,gColor2,fade);
    return c;
}

float4 PS(vs2ps In): SV_Target
{
    float4 col = texture2d.Sample(linearSampler,In.TexCd.xy) * cAmb;
	float4 gc=pGRAD(In.PosWVP, In.TexCd.xy);
	col = col * gc;
	return col;
}
technique10 Constant
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, VS() ) );
		SetPixelShader( CompileShader( ps_4_0, PS() ) );
	}
}




