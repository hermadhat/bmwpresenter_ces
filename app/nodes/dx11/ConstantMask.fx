//@author: ethermammoth
//@help: Alpha Mask Shader
//@tags: color
//@credits: 
struct vsInputTextured
{
    float4 posObject : POSITION;
	float4 uv: TEXCOORD0;
};

struct psInputTextured
{
    float4 posScreen : SV_Position;
    float4 uv: TEXCOORD0;
	float4 uvm: TEXCOORD1;
};

Texture2D inputTexture <string uiname="Texture";>;
Texture2D maskTexture <string uiname="Mask Texture";>;

SamplerState linearSampler <string uiname="Sampler State";>
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};

SamplerState alphaSampler <bool visible=false;string uiname="Sampler Alpha";> 
{
	Filter=MIN_MAG_MIP_LINEAR;
	AddressU=BORDER;
	AddressV=BORDER;
};

cbuffer cbPerDraw : register(b0)
{
	float4x4 tVP : VIEWPROJECTION;
};

cbuffer cbPerObj : register( b1 )
{
	float4x4 tW : WORLD;
	float Alpha <float uimin=0.0; float uimax=1.0;> = 1; 
	float4 cAmb <bool color=true;String uiname="Color";> = { 1.0f,1.0f,1.0f,1.0f };
	bool Original <string uiname="Keep Original Alpha";> =1;
	bool Invert=0;
};

cbuffer cbTextureData : register(b2)
{
	float4x4 tTex <string uiname="Texture Transform"; bool uvspace=true; >;
	float4x4 mTex <string uiname="Mask Texture Transform"; bool uvspace=true; >;
};

psInputTextured VS_Textured(vsInputTextured input)
{
	psInputTextured output;
	output.posScreen = mul(input.posObject,mul(tW,tVP));
	output.uv = mul(input.uv, tTex);
	output.uvm = mul(input.uv, mTex);
	return output;
}

float4 PS_Textured(psInputTextured input): SV_Target
{
    float4 col = inputTexture.Sample(linearSampler,input.uv.xy) * cAmb;
	float4 alphacut = maskTexture.SampleLevel(alphaSampler,input.uvm.xy,1);
	float a = saturate(alphacut.a);
	if(Invert)a=1-a;
	if(!Original)col.a=1;
	col.a *= a;
	col.a *= Alpha;
    return col;
}

technique11 Constant
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, VS_Textured() ) );
		SetPixelShader( CompileShader( ps_4_0, PS_Textured() ) );
	}
}





